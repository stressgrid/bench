#!/bin/bash

SGCLI="sgcli -c $COORDINATOR_HOST"
EXECUTE="ssh -i ~/sg.pem ubuntu@$SSH_HOST"
#EXECUTE="bash -c"

SUFFIX=""
#SUFFIX="-http2"
TARGET_PORT="5000"
PROTOCOL="http"
#PROTOCOL="http2"


SIZE="100000"

RAMPUP="3600"
SUSTAIN="60"
RAMPDOWN="60"

mkdir -p bench;
pushd bench;

for TARGET in $($EXECUTE "ls ~/targets"); do
  echo "Starting $TARGET..."
  $EXECUTE "cd ~/targets/$TARGET && ./start";
  sleep 10

  NAME="${TARGET//_/-}$SUFFIX"
  wget $($SGCLI run --target-hosts $TARGET_HOST --target-port $TARGET_PORT --target-protocol $PROTOCOL --size $SIZE --rampup $RAMPUP --sustain $SUSTAIN --rampdown $RAMPDOWN $NAME ../bench.ex)

  # curl $SSH_HOST:5000

  echo "Stopping $TARGET..."
  $EXECUTE "cd ~/targets/$TARGET && ./stop";
  sleep 10

  # curl $SSH_HOST:5000
done; 

popd;
